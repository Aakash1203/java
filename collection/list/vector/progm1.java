import java.util.*;
class VectorDemo {
	
	public static void main(String [] agrs){
		
		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);
		v.addElement(20);
		
		System.out.println(v);
		 
		// 1) public synchronized int lastIndexOf(java.lang.Object);
		System.out.println(v.lastIndexOf(v.get(1)));

		// 2)  public synchronized int lastIndexOf(java.lang.Object, int);
		System.out.println(v.lastIndexOf(30, 4));
		
				
		// 3) public synchronized E elementAt(int);
		System.out.println(v.elementAt(2));
		
		
		// 4) public synchronized E firstElement();
		System.out.println(v.firstElement());

		// 5) public synchronized E lastElement();
		System.out.println(v.lastElement());

		
		// 6) public synchronized void setElementAt(E, int);	
		v.setElementAt(50,4);
		System.out.println(v);
		
		// 7)  public synchronized void removeElementAt(int);
		v.removeElementAt(2);
		System.out.println(v);

		// 8)  public synchronized void insertElementAt(E, int);
		v.insertElementAt(60,3);
		System.out.println(v);
		
		
		// 9)  public synchronized boolean removeElement(java.lang.Object);
		System.out.println(v.removeElement(v.get(1)));
		
		// 10) public synchronized void removeAllElements();
		v.removeAllElements();
		System.out.println(v);
	}
}
