import java.util.*;
class LinkedListDemo{
	
	public static void main(String [] agrs){
	
		LinkedList ll = new LinkedList();

		ll.add("Aakash");
		ll.add("Patil");
		ll.add(21);
		System.out.println(ll);

		ll.add(1,"Anant");
		System.out.println(ll);


		// 1) public E getFirst();
			System.out.println(ll.getFirst());

  		// 2) public E getLast();
			System.out.println(ll.getLast());

		// 3) public E removeFirst();
			System.out.println(ll.removeFirst());

		// 4) public E removeLast();
			System.out.println(ll.removeLast());
							
		// 5) public void addFirst(E);
			ll.addFirst("Minal");
			System.out.println(ll);
					
		// 6) public void addLast(E);
			ll.addLast(20);
			System.out.println(ll);
	}
}
