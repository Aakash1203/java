/* print 'fizz' if it is divisible by 3
   print 'buzz' if it is divisible by 5
   print 'fizz-buzz' if it is divisible by by both 
*/

class Demo{
	public static void main(String [] Aakash){

		int x = 15;
		if(x%3==0 && x%5==0){
			System.out.println("Fizz-Buzz");
		}else if(x%5==0){
			System.out.println("Buzz");
		}else if(x%3==0){
			System.out.println("Fizz");
		}else 
			System.out.println("Not divisible by 3 and 5");
	}
}
