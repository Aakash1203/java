/* check the number is Armstrong or not
   Armstrong = eg.25 -->2*2 + 5*5 = sum 29 it is not armstrong no.
                 153 -->1*1*1 + 5*5*5 + 3*3*3 = sum 153
		 1634-->1*1*1*1 + 6*6*6*6 + 3*3*3*3 + 4*4*4*4 = sum 1634
*/

class Demo{
	public static void main(String [] Aakash){
	
		int n = 153;
		int temp1 =n;
		int temp2 =n;
		int sum = 0;
		int count =0;

		while(temp1!=0){
			count ++;
			temp1 = temp1/10;
		}
		int rem=0;
		while(temp2!=0){
			rem = temp2%10;
			temp2 = temp2/10;
			
			int mult =1;
			for(int i=1; i<=count; i++){
				mult = mult * rem;	
			}	
				sum = mult + sum;
		}		
		
		if(sum==n){
			System.out.println(n+" is Armstrong number");
		}else
			System.out.println(n+" is not Armstrong number");
	}
}
