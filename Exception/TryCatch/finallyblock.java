class FinallyDemo{

	void m1(){
		System.out.println("In m1");
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String [] agrs){
	
		System.out.println("Start Main");
		FinallyDemo obj = new FinallyDemo();
		obj.m1();
		obj = null;
		try{
			obj.m2();
		}
		catch(ArithmeticException ae){
			System.out.println("Here catch");
		}
		Final{
			System.out.println("Connection closed");
		}
		System.out.println("End Main");
	}
}
