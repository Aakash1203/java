import java.io.*;
class NumberformatDemo{

	public static void main(String [] agrs)throws IOException{
		
		System.out.println("Start Main");	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Name");
		String str = br.readLine();
		System.out.println(str);
		System.out.println("Enter Number");
		int data = 0;
		try{
			data = Integer.parseInt(br.readLine());
		}catch(NumberFormatException ne){
			System.out.println("Please Enter Valid Data");
			
			data = Integer.parseInt(br.readLine());
		}	
		System.out.println(data);
		System.out.println("End Main");	
	}
}
