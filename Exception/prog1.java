
import java.io.*;
class Demo{

	void m1(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter value of x :");
		int x = 0;
		try{
			 x = Integer.parseInt(br.readLine());
		}
		catch(IOException obj1){
			System.out.println("Good Program Aakash");
		}
		catch(NumberFormatException obj2){
			System.out.println("Bad Program");
		}
		
		System.out.println("x = " + x);
	}

	public static void main(String [] args){
		Demo obj = new Demo();
		obj.m1();
		

	}
}

