import java.util.*;
class InsufficientMoneyException extends RuntimeException{
	InsufficientMoneyException(String msg){
		super(msg);
	}
}
class Transaction{
	public static void main(String [] agrs){
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Amount");
		int Amount = sc.nextInt();
		
		if(Amount>500)
			throw new InsufficientMoneyException("Bank Account Madhe paise nhi ahe");
	}
}
