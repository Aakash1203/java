/*
 we can write obj as null but if we try to perform operation using null obj then it will give null pointer exception
*/

class NullPointerDemo{
	void m1(){		
		System.out.println("In m1");
	}
	void m2(){
		System.out.println("In m2");
	}
}
class Client{
	public static void main(String [] agrs){
		
		System.out.println("Start main");
		NullPointerDemo obj = new NullPointerDemo();
		obj.m1();
		obj = null;
		obj.m2();
		System.out.println("End main");

	}
}

/*
Output :-
	Start main
	In m1
	Exception in thread "main" java.lang.NullPointerException: Cannot invoke "NullPointerDemo.m2()" because "<local1>" is null
		at Client.main(nullpointer.java:16)
*/

