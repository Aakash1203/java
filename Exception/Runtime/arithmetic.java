/* ArithmeticException = is runtime exception... for divide by zero*/

class ArithmeticDemo{
	void m1(){
		System.out.println(10/0);
		m2();
	}
	void m2(){
		System.out.println("In m2");
	}		
}
class Client{
	public static void main(String [] args){
		
		ArithmeticDemo obj = new ArithmeticDemo();
		obj.m1();
	}
}


/*
Output :
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at ArithmeticDemo.m1(arithmetic.java:3)
	at Client.main(arithmetic.java:14)
*/
