import java.io.*;
class NumberFormatDemo{
	void m1(){
		System.out.println("In m1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		int x=0;
		try{
			x = Integer.parseInt(br.readLine());
		}
		catch(IOException obj){
			System.out.println(" In catch IoException");
		}
	}
	void m2(){
		System.out.println("In m2");
	}
	
}
class Client{
	public static void main(String [] agrs){
		
		System.out.println("start main");
		NumberFormatDemo obj = new NumberFormatDemo();
		obj.m1();
		obj.m2();
		System.out.println("End main");
	}
}
/*
Output :-
	start main
	In m1
	agh
	Exception in thread "main" java.lang.NumberFormatException: For input string: "agh"
		at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
		at java.base/java.lang.Integer.parseInt(Integer.java:668)
		at java.base/java.lang.Integer.parseInt(Integer.java:786)
		at NumberFormatDemo.m1(numberformat.java:9)
		at Client.main(numberformat.java:25)
*/
