/* 	Write a Java program to find the sum of even and odd numbers in an array.
	Display the sum value.
	Input: 11 12 13 14 15
	Output
	Odd numbers sum = 39
	Even numbers sum = 26
*/

import java.io.*;
class ArrayDemo{
	public static void main(String [] args)throws IOException{
		
		int sumeven = 0; int sumodd = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre The Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Entre The Array Element");
		
		for(int i=0; i<arr.length; i++){
				
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				sumeven = sumeven + arr[i];
			}
			else{
				sumodd = sumodd + arr[i];
			}
		}	
		
		System.out.println("Even numbers sum = "+ sumeven);
		System.out.println("Odd numbers sum = "+ sumodd);
	}
}
