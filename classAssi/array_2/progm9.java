/* 	Write a Java program to merge two given arrays.
	Array1 = [10, 20, 30, 40, 50]
	Array2 = [9, 18, 27, 36, 45]
	Output :
	Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
	Hint: you can take 3rd array
*/

import java.io.*;
class ArrayDemo{
	public static void main(String [] args)throws IOException{
		
		int sum = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre The Array1 Size");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		
		System.out.println("Entre The Array1 Element");
		
		for(int i=0; i<arr1.length; i++){			
			arr1[i] = Integer.parseInt(br.readLine());			
		}
	        System.out.println("Entre The Array2 Size");
                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Entre The Array2 Element");

                for(int i=0; i<arr2.length; i++){
                        arr2[i] = Integer.parseInt(br.readLine());
                }
		int arr3[] = new int[size1 + size2];
		for(int i=0; i<arr1.length; i++){			
					
			arr3[i]=arr1[i];
		}
		for(int i=0; i<arr2.length; i++){			
			arr3[size1 + i]=arr2[i];
		}
		
		System.out.println("Merged Array is ");
		for(int i=0; i<arr3.length; i++){			
			System.out.println(arr3[i]);
		}
	}
}
