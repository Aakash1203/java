/* 	WAP to find the number of even and odd integers in a given array of integers
	Input: 1 2 5 4 6 7 8
	Output:
	Number of Even Elements: 4
	Number of Odd Elements : 3
*/

import java.io.*;
class ArrayDemo{
	public static void main(String [] args)throws IOException{
		
		int count = 0; int count1 = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre The Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Entre The Array Element");
		
		for(int i=0; i<arr.length; i++){
				
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				count++;
			}
			else{
				count1++;
			}
		}
		
		System.out.println("Number of Even Elements  = "+ count);
		System.out.println("Number of Odd Elements  = "+ count1);
	}
}
