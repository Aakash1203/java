/*
  Q.4 	Write a program, and take two characters if these characters are equal then print them as it is but if
	they are unequal then print their difference.
	{Note: Consider Positional Difference Not ASCIIs}    
								(A-Z = 65-90)
	Input: a p						(a-z = 97-122)
	Output: The difference between a and p is 15
*/

import java.io.*;
class Demo{
	public static void main(String[] Aakash)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre 1st char");
		char ch1 = (char)br.read();
		
		br.skip(1);
		System.out.println("Entre 2nd char");
		char ch2 = (char)br.read();
		 
		int count =0;

		if(ch1>ch2){
			for(int i=ch2;i<ch1;i++){
				count++;
			}
		}else{
			for(int i=ch1;i<ch2;i++){
				count++;
			}
		}
		System.out.println("difference between "+ ch1 +" and "+ch2+" is "+ count);
	}
}
