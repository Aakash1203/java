/*  using bufferedreader 
Q.2 
	O			  10
	14 13			  I  H
	L  K  J			  7  6  5
	9  8  7  6		  D  C  B  A	
	E  D  C  B  A
*/

import java.io.*;
class Demo{

	public static void main(String[] Aakash)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre the Row");
		int row=Integer.parseInt(br.readLine());
		
		int val = (row *(row+1))/2;
		char ch = (char)(64+val);
		int rwval = row;
		        
		for(int i=1;i<=row;i++){
		               
			for(int j=1;j<=i;j++){
				if(rwval%2 !=0){
					System.out.print(ch + " ");
				}else{
					System.out.print(val + " ");
				}
				val--;ch--;
			}
			System.out.println();
			rwval--;
		}
	}
}

