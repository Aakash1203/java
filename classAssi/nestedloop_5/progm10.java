/*   Q.10
	prime no.
	input = Entre the starting no = 10
		Entre the end no = 100
	output = 11 13 17 19 ..... 89 97
   
*/
import java.io.*;
class Demo{
	public static void main(String[] Aakash)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Entre the Starting Number");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Entre the End Number");
		int end = Integer.parseInt(br.readLine());
	
		for(int i=start; i<=end; i++){
			int count = 0;
			for(int j=1; j<=i; j++){
				if(i%j==0){
					count++;
				}
			}
			if(count==2){
				System.out.print(i+" ");
			}
		}
		System.out.println();
	}
}
