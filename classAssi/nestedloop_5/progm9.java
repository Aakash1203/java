/*
    Q.9   input  = 1234
	  output = Additon of factorials of each digit from 1234 = 33
*/

import java.io.*;
class Demo{
	public static void main(String[] Aakash)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre The Number");
		int num = Integer.parseInt(br.readLine());
		int num1 = num;		
		int sum = 0;
		while(num!=0){
			int rem=num%10;
			int fact = 1;
			for(int i=1; i<=rem; i++){
			
				fact = fact*i;
			}
			sum = sum+fact;
			num = num/10;
		}
		System.out.println("Addition of factorial of each digit from "+num1+" = "+sum );
	}
}
