/* 
	$
	@  @
	&  &  &
	#  #  #  #
        $  $  $  $  $
        @  @  @  @  @  @
        &  &  &  &  &  &  &
        #  #  #  #  #  #  #  #
*/

import java.io.*;
class Demo{
	public static void main(String[] Aakash)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre the row");
		int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
	
			for(int j=1;j<=i;j++){
			
				if(i==1 || row-i==3){
					System.out.print("$");
				}
				if(i==2 || row-i==2){
					System.out.print("@");
				}
				if(i==3 || row-i==1){
					System.out.print("&");
				}
				if(i==4 || row-i==0){
                                        System.out.print("#");
                                }
                        }       		
			System.out.println();
			
		}
	}
}
