// Write a program in which students should enter marks of 5 different subjects. If all subject
//  having above passing marks add them and provide to switch case to print grades(first class
//  second class), if student get fail in any subject program should print “You failed in exam”

import java.util.*;
class SwitchDemo{
	public static void main(String[] Aakash){
		
		Scanner obj= new Scanner(System.in);

		System.out.println("Entre DBMS marks: ");
		float dbms= obj.nextFloat();
		
		System.out.println("Entre CNS marks: ");
                float cns= obj.nextFloat();
		
		System.out.println("Entre SPOS marks: ");
                float spos= obj.nextFloat();
		
		System.out.println("Entre TOC marks: ");
                float toc= obj.nextFloat();
		
		System.out.println("Entre IOT marks: ");
                float iot= obj.nextFloat(); 
	
		if(dbms>=28 && cns>=28 && spos>=28 && toc>=28 && iot>=28){
				
			float sum= dbms+cns+spos+toc+iot;
			char grade;

			if(sum>=300)
				grade='A';
			else
				grade='B';
			
			switch(grade){
				
				case 'A':
					System.out.println("First Class");
					System.out.println("Total Marks= "+sum);
					break;
				case 'B':
					System.out.println("Second Class");
					System.out.println("Total Marks= "+sum);
					break;
			}
			

		}else 
			System.out.println("You Failed in Exam ");

		
	}
}
