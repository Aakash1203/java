/* WAP to take size of the array from the user and also take integer elements from the user
   Print the sum of odd elements only
   input: Enter size: 5
   Enter array elements: 1 2 3 4 5
   output: 9
   //1 + 3 + 5
*/


import java.io.*;
class ArrayDemo{
	public static void main(String[] Aakash)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre the Array size");
		int size = Integer.parseInt(br.readLine());
		
		int sum =0;
		int arr[] = new int[size];
		System.out.println("Entre Array elements");
		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2==1){
				sum = sum + arr[i];
				}
		}
		System.out.println("The sum of odd elements " +sum);
		
	}
}
