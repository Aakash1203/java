/* WAP to take size of array from user and also take integer elements from user Print
   product of even elements only
   input : Enter size : 9
   Enter array elements : 1 2 3 2 5 10 55 77 99
   output : 40
   // 2 * 2 * 10
*/


import java.io.*;
class ArrayDemo{
	public static void main(String[] Aakash)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Entre the Array size");
		int size = Integer.parseInt(br.readLine());
		
		int mul =1;
		int arr[] = new int[size];
		System.out.println("Entre Array elements");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				mul = mul * arr[i];
				}
		}
		System.out.println("The product of even  elements " +mul);
		
	}
}
