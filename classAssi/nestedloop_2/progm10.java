/*  print pattern = F 5 D 3 B 1 
		    F 5 D 3 B 1 
		    F 5 D 3 B 1
		    F 5 D 3 B 1 
		    F 5 D 3 B 1
		    F 5 D 3 B 1
		    
		     		    		   		   	      
		    
*/

class Demo{
	public static void main(String [] Aakash){

		
		for(int i=1; i<=6; i++){
			
			 char ch ='F';
                         int val = 6; 	
			for(int j=1; j<=6; j++){
				
				if(j%2!=0){
					System.out.print(ch+"\t");
			        }
				else{
					System.out.print(val+ "\t");
				}
				ch--; 
				val--;
			}
			System.out.println();
		}
	}
}
