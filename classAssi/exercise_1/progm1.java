/*	Write a Program to Print following Pattern. note: take rows from user.
	A  C  E  G
	B  D  F 
	C  E
	D
*/
import java.io.*;
class Demo{
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int row = Integer.parseInt(br.readLine());

		for(int i=1; i<=row; i++){
			char ch = (char)(64+i);
			for(int j=1; j<=(row-i+1); j++){
				
				System.out.print(ch+" ");
				ch = (char)(ch+2);
			}
			System.out.println();
		}
	}
}
