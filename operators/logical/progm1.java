/* && , || , ^ */
class logical{
	public static void main(String [] args){
		int x = 10;
		int y = 20;
		
		boolean ans = (x<y) && (y>x);
		boolean ans1 = (x>y) || (y>x);
		boolean ans2 = (x>y) ^ (x<y);

		System.out.println("ans = "+ ans);
		System.out.println("ans = "+ ans1);
		System.out.println("ans = "+ ans2);
	}
}
