/*Bitwise Operater &,|,^,<<,>>,>>>,~   Binary Value
  &(AND => 0  0 = 0     |(OR) => 0  0 = 0      ^(XOR) => 0  0 = 0
           0  1 = 0              0  1 = 1                0  1 = 1
           1  0 = 0              1  0 = 1                1  0 = 1
           1  1 = 1              1  1 = 1                1  1 = 0
      
 */

class Demo{
	public static void main(String [] Aakash){
		int a = 5;
		int b = 7;
		int c =14;
		int d =9;

		System.out.println( a & b);
		System.out.println( a | b);
		System.out.println( c & d);
		System.out.println( c | d);
	}
}
