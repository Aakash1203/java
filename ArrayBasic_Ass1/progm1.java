/* 1] Search an Element in an array

   Given an integer array and another integer element. The task is to find if the given
   element is present in the array or not.
*/
import java.io.*;
class ArrayDemo{

        public static void main(String [] agrs)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the Size Of Array");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.println("Enter the Elements");
                for(int i=0; i<arr.length; i++){
                        arr[i] = Integer.parseInt(br.readLine());
                }
                System.out.println("Enter Finding Element");
                int search = Integer.parseInt(br.readLine());
                for(int i=0; i<arr.length; i++){
                        if(search==arr[i]){
                                System.out.println(i);
                        }
                }

        }
}
