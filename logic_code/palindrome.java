import java.util.*;
class Demo{
	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int temp = num;
		int rev = 0;   	

		while(temp!=0){
			
			int rem = temp%10;
			rev = rev * 10 + rem ;
			temp = temp/10;
		}
		if(rev==num){
			System.out.println("Number Is Palindrome = "+ num);
		}
		else
			System.out.println("Number is not Palindrome = "+ num);
	}

}
