/* 	charAt(int index)
	Returns the char value at the specified index.
*/

class CharAtDemo{
	public static void main(String [] args){
	
		String str1 = "Aakash";
	
		System.out.println(str1.charAt(3));	
		System.out.println(str1.charAt(5));	
		System.out.println(str1.charAt(0));	
	}
}
