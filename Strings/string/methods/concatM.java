/*   	public String concat(String str)
Concatenates the specified string to the end of this string.
If the length of the argument string is 0, then this String object is returned. Otherwise, a String object is returned that represents a character sequence that is the concatenation of the character sequence represented by this String object and the character sequence represented by the argument string.

Examples:

 "cares".concat("s") returns "caress"
 "to".concat("get").concat("her") returns "together"
 
Parameters:	str - the String that is concatenated to the end of this String.
Returns: a string that represents the concatenation of this object's characters followed by the string argument's characters.
*/

class ConcatDemo{
	public static void main(String [] args){
	
		String str1 = "Aakash";
		String str2 = "Patil";
		String str3 = new String(str2);
		System.out.println(str3);
		
		str3 = str3 + "aakash";
		str2 = str2.concat(str1);

		System.out.println(str3);
		System.out.println(str2);
	}
}
