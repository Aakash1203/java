class Append{
	public static void main(String [] args){
		
		StringBuffer sb = new StringBuffer("Shashi");
		StringBuffer sb1 = new StringBuffer();
		
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());

                System.out.println(sb1);
                System.out.println(System.identityHashCode(sb1));
		System.out.println(sb1.capacity());
		
		sb.append("Core2web");
		sb1.append("Bagal");

		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());

                System.out.println(sb1);
                System.out.println(System.identityHashCode(sb1));
		System.out.println(sb1.capacity());
	}
}
