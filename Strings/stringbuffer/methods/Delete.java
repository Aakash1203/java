class Delete{
	public static void main(String [] args){
		
		StringBuffer sb = new StringBuffer("Core2web");
		
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());
		sb.delete(2,7);       // here 2 - from givien index deleting the string through
				      // here 7 - (7-1=6) 
		System.out.println(sb);
                System.out.println(System.identityHashCode(sb));
                System.out.println(sb.capacity());

		sb.delete(0,1);
		System.out.println(sb);
		
	}
}
