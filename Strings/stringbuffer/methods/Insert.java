class Insert{
	public static void main(String [] args){
	
		StringBuffer sb = new StringBuffer("ShashiCore2web");

		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
		
		sb.insert(6,"Bagal");    // 6 - postion of string from where we want to add 
					 // Bagal - String add to this postion
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
	}
}
