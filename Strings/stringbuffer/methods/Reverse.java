class Reverse{
	public static void main(String [] args){
		
		StringBuffer sb = new StringBuffer("Shashi");
		System.out.println(sb.reverse());
		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());
		
		String str1 = "Core2web";
		StringBuffer sb1 = new StringBuffer(str1);
		str1 = sb1.reverse().toString();   // here toString convert stringbuffer into the string 
		System.out.println(str1);

		System.out.println(System.identityHashCode(str1));

                System.out.println(System.identityHashCode(sb1));
                System.out.println(sb1.capacity());
		
		// without using reverse methods

		String str = "Aakash";
		String reve = "";
		for(int i=str.length()-1 ; i>=0 ; i--){
			reve = reve + str.charAt(i);
		}
		System.out.println(reve);
	}
}
